    MDR ACC AGE INCOME  AVGEXP OWNRENT SELFEMPL
1     0   1  38   4.52  124.98       1        0
2     0   1  33   2.42    9.85       0        0
3     0   1  34   4.50   15.00       1        0
4     0   1  31   2.54  137.87       0        0
5     0   1  32   9.79  546.50       1        0
6     0   1  23   2.50   92.00       0        0
7     0   1  28   3.96   40.83       0        0
8     0   1  29   2.37  150.79       1        0
9     0   1  37   3.80  777.82       1        0
10    0   1  28   3.20   52.58       0        0
11    0   1  31   3.95  256.66       1        0
12    0   0  42   1.98    0.00       1        0
13    0   0  30   1.73    0.00       1        0
14    0   1  29   2.45   78.87       1        0
15    0   1  35   1.91   42.62       1        0
16    0   1  41   3.20  335.43       1        0
17    0   1  40   4.00  248.72       1        0
18    7   0  30   3.00    0.00       1        0
19    0   1  40  10.00  548.03       1        1
20    3   0  46   3.40    0.00       0        0
21    0   1  35   2.35   43.34       1        0
22    1   0  25   1.88    0.00       0        0
23    0   1  34   2.00  218.52       1        0
24    1   1  36   4.00  170.64       0        0
25    0   1  43   5.14   37.58       1        0
26    0   1  30   4.51  502.20       0        0
27    0   0  22   3.84    0.00       0        1
28    0   1  22   1.50   73.18       0        0
29    0   0  34   2.50    0.00       1        0
30    0   1  40   5.50 1532.77       1        0
31    0   1  22   2.03   42.69       0        0
32    1   1  29   3.20  417.83       0        0
33    1   0  25   3.15    0.00       1        0
34    0   1  21   2.47  552.72       1        0
35    0   1  24   3.00  222.54       0        0
36    0   1  43   3.54  541.30       1        0
37    0   0  43   2.28    0.00       0        0
38    0   1  37   5.70  568.77       1        0
39    0   1  27   3.50  344.47       0        0
40    0   1  28   4.60  405.35       1        0
41    0   1  26   3.00  310.94       1        0
42    0   1  23   2.59   53.65       0        0
43    0   1  30   1.51   63.92       0        0
44    0   1  30   1.85  165.85       0        0
45    0   1  38   2.60    9.58       0        0
46    0   0  28   1.80    0.00       0        1
47    0   1  36   2.00  319.49       0        0
48    0   0  38   3.26    0.00       0        0
49    0   1  26   2.35   83.08       0        0
50    0   1  28   7.00  644.83       1        0
51    0   0  50   3.60    0.00       0        0
52    0   1  24   2.00   93.20       0        0
53    0   1  21   1.70  105.04       0        0
54    0   1  24   2.80   34.13       0        0
55    0   1  26   2.40   41.19       0        0
56    1   1  33   3.00  169.89       0        0
57    0   1  34   4.80 1898.03       0        0
58    0   1  33   3.18  810.39       0        0
59    0   0  45   1.80    0.00       0        0
60    0   1  21   1.50   32.78       0        0
61    2   1  25   3.00   95.80       0        0
62    0   1  27   2.28   27.78       0        0
63    0   1  26   2.80  215.07       0        0
64    0   1  22   2.70   79.51       0        0
65    3   0  27   4.90    0.00       1        0
66    0   0  26   2.50    0.00       0        1
67    0   1  41   6.00  306.03       0        1
68    0   1  42   3.90  104.54       0        0
69    0   0  22   5.10    0.00       0        0
70    0   1  25   3.07  642.47       0        0
71    0   1  31   2.46  308.05       1        0
72    0   1  27   2.00  186.35       0        0
73    0   1  33   3.25   56.15       0        0
74    0   1  37   2.72  129.37       0        0
75    0   1  27   2.20   93.11       0        0
76    1   0  24   4.10    0.00       0        0
77    0   1  24   3.75  292.66       0        0
78    0   1  25   2.88   98.46       0        0
79    0   1  36   3.05  258.55       0        0
80    0   1  33   2.55  101.68       0        0
81    0   0  33   4.00    0.00       0        0
82    1   1  55   2.64   65.25       1        0
83    0   1  20   1.65  108.61       0        0
84    0   1  29   2.40   49.56       0        0
85    3   0  40   3.71    0.00       0        0
86    0   1  41   7.24  235.57       1        0
87    0   0  41   4.39    0.00       1        0
88    0   0  35   3.30    0.00       1        0
89    0   0  24   2.30    0.00       0        0
90    1   0  54   4.18    0.00       0        0
91    2   0  34   2.49    0.00       0        0
92    0   0  45   2.81    0.00       1        0
93    0   1  43   2.40   68.38       0        0
94    4   0  35   1.50    0.00       0        0
95    2   0  36   8.40    0.00       0        0
96    0   1  22   1.56    0.00       0        0
97    1   1  33   6.00  474.15       1        0
98    1   1  25   3.60  234.05       0        0
99    0   1  26   5.00  451.20       1        0
100   0   1  46   5.50  251.52       1        0
[1] ""
[1] ""
[1] ""
[1] "Frequency Table ACC"
accTable
 0  1 
27 73 
[1] "Relative Frequency ACC"
accTable
   0    1 
0.27 0.73 
null device 
          1 
null device 
          1 
[1] ""
[1] ""
[1] ""
[1] "Frequency Table OwnRent"
ownrentTable
 0  1 
64 36 
[1] "Relative Frequency Ownrent"
ownrentTable
   0    1 
0.64 0.36 
null device 
          1 
null device 
          1 
[1] ""
[1] ""
[1] ""
[1] "Frequency Table Ownrent"
selfemplTable
 0  1 
95  5 
[1] "Relative Frequency selfempl"
selfemplTable
   0    1 
0.95 0.05 
null device 
          1 
null device 
          1 
[1] ""
[1] ""
[1] ""
[1] "Absolute Frequency MDR"
mdrTable
 0  1 
82 18 
[1] "Relative Frequency mdr"
mdrTable
   0    1 
0.82 0.18 
null device 
          1 
null device 
          1 
[1] ""
[1] ""
[1] ""
[1] ""
[1] ""
[1] ""
[1] "Age Frequency Table"
ageTable
20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 40 41 42 43 45 46 50 
 1  3  6  2  6  6  6  5  5  4  5  3  1  7  5  4  4  3  3  4  4  2  4  2  2  1 
54 55 
 1  1 
[1] "Mean of age"
  AGE 
32.08 
[1] "Median of age"
[1] 31
[1] "Mode of age"
[1] 33
[1] "Summary of age"
      AGE       
 Min.   :20.00  
 1st Qu.:26.00  
 Median :31.00  
 Mean   :32.08  
 3rd Qu.:37.00  
 Max.   :55.00  
[1] "Variance of age"
[1] 60.6736
[1] "Standard Deviation of age"
[1] 7.789326
[1] "Coefficients of variation of age"
     AGE 
24.28094 
[1] "Range of age"
[1] 35
[1] "Interquartilic range of age"
75% 
 11 
[1] "Fishers asymmetry coefficient for Age"
[1] 0.001347341
[1] "Pearsons asymmetry coefficient for Age"
       AGE 
-0.1181103 
[1] ""
[1] ""
[1] ""
[1] "Income Frequency Table"
incomeTable
 1.5 1.51 1.56 1.65  1.7 1.73  1.8 1.85 1.88 1.91 1.98    2 2.03  2.2 2.28  2.3 
   3    1    1    1    1    1    2    1    1    1    1    4    1    1    2    1 
2.35 2.37  2.4 2.42 2.45 2.46 2.47 2.49  2.5 2.54 2.55 2.59  2.6 2.64  2.7 2.72 
   2    1    3    1    1    1    1    1    3    1    1    1    1    1    1    1 
 2.8 2.81 2.88    3 3.05 3.07 3.15 3.18  3.2 3.25 3.26  3.3  3.4  3.5 3.54  3.6 
   2    1    1    5    1    1    1    1    3    1    1    1    1    1    1    2 
3.71 3.75  3.8 3.84  3.9 3.95 3.96    4  4.1 4.18 4.39  4.5 4.51 4.52  4.6  4.8 
   1    1    1    1    1    1    1    3    1    1    1    1    1    1    1    1 
 4.9    5  5.1 5.14  5.5  5.7    6    7 7.24  8.4 9.79   10 
   1    1    1    1    2    1    2    1    1    1    1    1 
[1] "Sum of relative Frequency of income"
[1] 18 60 17  2  3
[1] "Relative frequency of income"
[1] 0.18 0.60 0.17 0.02 0.03
[1] "High of histogram bars of income"
[1] 0.090 0.300 0.085 0.010 0.015
[1] "Income mean"
INCOME 
3.3693 
[1] "Income Median"
[1] 3
[1] "Mode of Income"
[1] 3
[1] "Summary of income"
     INCOME      
 Min.   : 1.500  
 1st Qu.: 2.365  
 Median : 3.000  
 Mean   : 3.369  
 3rd Qu.: 3.970  
 Max.   :10.000  
[1] "Variance of income"
[1] 2.627147
[1] "Standard Deviation of income"
[1] 1.620847
[1] "Coefficients of variation of income"
  INCOME 
48.10636 
[1] "Range of income"
[1] 8.5
[1] "Interquartilic range of income"
  75% 
1.605 
[1] "Fishers asymmetry coefficient for income"
[1] 1.853352
[1] "Pearsons asymmetry coefficient for income"
   INCOME 
0.2278438 
null device 
          1 
[1] ""
[1] ""
[1] ""
[1] "AvgExpo Frequency Table"
avgExpoTable
   9.58    9.85      15   27.78   32.78   34.13   37.58   40.83   41.19   42.62 
      1       1       1       1       1       1       1       1       1       1 
  42.69   43.34   49.56   52.58   53.65   56.15   63.92   65.25   68.38   73.18 
      1       1       1       1       1       1       1       1       1       1 
  78.87   79.51   83.08      92   93.11    93.2    95.8   98.46  101.68  104.54 
      1       1       1       1       1       1       1       1       1       1 
 105.04  108.61  124.98  129.37  137.87  150.79  165.85  169.89  170.64  186.35 
      1       1       1       1       1       1       1       1       1       1 
 215.07  218.52  222.54  234.05  235.57  248.72  251.52  256.66  258.55  292.66 
      1       1       1       1       1       1       1       1       1       1 
 306.03  308.05  310.94  319.49  335.43  344.47  405.35  417.83   451.2  474.15 
      1       1       1       1       1       1       1       1       1       1 
  502.2   541.3   546.5  548.03  552.72  568.77  642.47  644.83  777.82  810.39 
      1       1       1       1       1       1       1       1       1       1 
1532.77 1898.03 
      1       1 
[1] "Sum of relative Frequency of avgExpo"
 [1] 40 16 10  3  1  0  0  1  0  1
[1] "Relative frequency of avgExpo"
 [1] 0.55555556 0.22222222 0.13888889 0.04166667 0.01388889 0.00000000
 [7] 0.00000000 0.01388889 0.00000000 0.01388889
[1] "High of histogram bars of avgExpo"
 [1] 2.777778e-03 1.111111e-03 6.944444e-04 2.083333e-04 6.944444e-05
 [6] 0.000000e+00 0.000000e+00 6.944444e-05 0.000000e+00 6.944444e-05
[1] "AvgExp mean"
[1] 262.5321
[1] "AvgExp "
[1] 158.32
[1] "Mode of avgExpo"
[1] 9.58
[1] "Summary of AvgExpo"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   9.58   67.60  158.30  262.50  323.50 1898.00 
[1] "Variance of avgExpo"
[1] 99748.87
[1] "Standard Deviation of avgExpo"
[1] 315.8305
[1] "Coefficients of variation AvgExpo"
[1] 120.3017
[1] "Range of AvgExpo"
[1] 1888.45
[1] "Interquartilic ranges of avg expo"
     75% 
255.8775 
[1] "Fishers asymmetry coefficient for avgExpo"
[1] 2.9584
[1] "Pearsons asymmetry coefficient for avgExpo"
[1] 0.800911
null device 
          1 
null device 
          1 
[1] "Both quantitative avg-expo vs income covariance"
[1] 0.2359105
[1] "Both quantitative avg-expo vs income correlation"
[1] 0.4994236
null device 
          1 
[1] "regression for income avg-expo"
       (Intercept) log(uAvgExpoTable) 
         0.2025952          0.1873225 
[1] "Comparison of means between income with ACC"
       0        1 
3.255556 3.411370 
[1] "Comparison of std between income with ACC"
       0        1 
1.437188 1.701849 
[1] "Comparison of variance between income with acc"
      0       1 
2.06551 2.89629 
[1] "coefficient variance of income with acc"
       0        1 
44.14572 49.88755 
null device 
          1 
null device 
          1 
[1] "Comparison of means between income with mdr"
       0        1 
3.302195 3.675000 
[1] "Comparison of std between income with mdr"
       0        1 
1.642459 1.574649 
[1] "Comparison of variance between income with mdr"
       0        1 
2.697672 2.479521 
[1] "coefficient variance of income with mdr"
      0       1 
49.7384 42.8476 
null device 
          1 
null device 
          1 
[1] "Bivariable absolute frenquency table of ACC and selfempl"
   
     0  1
  0 24  3
  1 71  2
[1] "Relative frequency of ACC and selfempl"
   
     0  1
  0 24  3
  1 71  2
[1] "Conditional between acc and self empl"
   
            0         1
  0 88.888889 11.111111
  1 97.260274  2.739726
[1] "Conditional between acc and self empl 2"
   
           0        1
  0 25.26316 74.73684
  1 60.00000 40.00000
null device 
          1 
null device 
          1 
null device 
          1 
[1] "Bivariable absolute frenquency table of ACC and ownRent"
   
     0  1
  0 18  9
  1 46 27
[1] "Relative frequency of ACC and selfempl"
   
     0  1
  0 18  9
  1 46 27
[1] "Conditional between acc and ownrent"
   
           0        1
  0 66.66667 33.33333
  1 63.01370 36.98630
null device 
          1 
summary statistics
------
min:  1.5   max:  10 
median:  3 
mean:  3.3693 
estimated sd:  1.629013 
estimated skewness:  1.881696 
estimated kurtosis:  7.579032 
null device 
          1 
